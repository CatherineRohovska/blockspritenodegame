//
//  Action.swift
//  BlockSpriteSwiftGame
//
//  Created by Admin on 1/22/16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import SpriteKit
enum ActionType{
case Destroy
case Move
case Pause
}

struct ArrPoint {
    var rowIndex: Int
    var colIndex: Int
}

class Action {
    var type: ActionType
    {
        didSet{
            if type == .Destroy || type == .Pause
            {
                duration = 0.2
            }
            else
            {
                duration = 1
            }
        }
    }
    var element: Element
    var nextPosition: ArrPoint
    var duration: Double
    var finalized = false
    var waitBeforeDestroy = 0.0
    init()
    {
        nextPosition = ArrPoint(rowIndex: 0,colIndex: 0)
        duration = 0.0
        type = .Move
 
    element = Element()
    }
    /**
     Create finalize action -- the result of makin gaction

     */
    func finalize()
    {
        if type == .Move
        {
//            print ("//---\\\\")
//            if element.rowIndex - self.nextPosition.rowIndex != 0
//            {
//             
//            print("Vertical")
//                print("Move from: ", element.rowIndex, "--", element.colIndex, " to ", self.nextPosition.rowIndex,"--", self.nextPosition.colIndex)
//            }
//            if element.colIndex - self.nextPosition.colIndex != 0
//            {
//            print("Horizontal")
//                print("Move from: ", element.rowIndex, "--", element.colIndex, " to ", self.nextPosition.rowIndex,"--", self.nextPosition.colIndex)
//            }
//            
//             print ("\\\\---//")
        element.rowIndex = self.nextPosition.rowIndex
        element.colIndex = self.nextPosition.colIndex
       
        }
        if type == .Destroy
        {
            //destroy one block costs certain score
            GameManager.sharedManager.currentScore += 5
            //
            element.removeFromParent()
           
        }
        if type == .Pause
        {
          element.removeFromParent()
        }
         finalized = true
    }
    /**
     Create action for element that can be applied to sprite
     @return action itself
     */
    func createActionForElement()->SKAction
    {
        var action = SKAction()
        if type == .Destroy
        {
            action = SKAction.sequence([SKAction.waitForDuration(waitBeforeDestroy), SKAction.animateWithTextures(element.removeAnimation, timePerFrame: 0.2/Double(element.removeAnimation.count))])
        }
        if type == .Move
        {
            let xDelta = nextPosition.colIndex - element.colIndex 
            let yDelta = element.rowIndex - nextPosition.rowIndex
            //            print("-Movement delta-")
            //            print(xDelta)
            //            print(yDelta)
            //            print("---")
           
            action = SKAction.moveTo(CGPoint(x: element.position.x + CGFloat(xDelta)*element.size.width, y: element.position.y + CGFloat(yDelta)*element.size.width), duration: 1)

        }
        if type == .Pause
        {
            print("Duration to destroy", duration)
            let newEmpty = Element()
            newEmpty.position = CGPoint(x: 0,y: 0)
            newEmpty.size = CGSize(width: 2, height: 2)
            element.parent?.addChild(newEmpty)
            element = newEmpty
            action = SKAction.waitForDuration(duration)

        }
        return action
    }
    
}
