//
//  GameManager.swift
//  BlockSpriteSwiftGame
//
//  Created by Admin on 1/20/16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit

class GameManager {
    static let sharedManager = GameManager()
    let recordKeyName = "recordScore"
    let savedScoreName = "savedScore"
    var currentScore: Int = 0
    {
        didSet
        {
            if currentScore > recordScore
            {
                recordScore = currentScore
            }
            print ("score ", currentScore)
        }
    }
    var recordScore: Int
    {
        didSet
        {
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setInteger(recordScore, forKey: recordKeyName)
            print("global score changed to: ", recordScore)
        }
        
    }
    private  init()
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        if defaults.integerForKey(recordKeyName) != 0
        {
            recordScore = defaults.integerForKey(recordKeyName)
        }
        else
        {
            recordScore = 0
        }
    }
    /**
    Save current game state
     
     */
    func saveLevel()
    {
        //save score
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(currentScore, forKey: savedScoreName)
        //
        var result = ""
        var singleString = "" //string to save information about particular element
        for var i = 0; i < SpritesManager.sharedManager.numRows; i++
        {
            for var j = 0; j < SpritesManager.sharedManager.numCols; j++
            {
                let element = SpritesManager.sharedManager.gameBoard[i][j]
                singleString = SpritesManager.sharedManager.typeOfElement(element)
                if singleString != ""
                {
                    singleString = singleString + " " + String(element.colIndex) + " " + String(element.rowIndex)
                    // if element is usual or scissors we add information about type (for usual) or direction (for scissors)
                    if SpritesManager.sharedManager.typeOfElement(element) == "UsualElement"
                    {
                        let usual = element as! UsualElement
                        singleString = singleString + " " + String(usual.type.rawValue)
                        
                        
                    }
                    if SpritesManager.sharedManager.typeOfElement(element) == "ScissorsElement"
                    {
                        let usual = element as! ScissorsElement
                        singleString = singleString + " " + String(usual.direction.rawValue)
                        
                        
                    }
                   
                    result = result + singleString + "\n"
                }
            }
        }
         print(result)
       // to file
        
        let file = "saved.txt" //file to write
        
        
        if let dir : NSString = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first
        {
            let path = dir.stringByAppendingPathComponent(file);
            
            //writing
            do
            {
                try result.writeToFile(path, atomically: false, encoding: NSUTF8StringEncoding)
            }
            catch
            {
                print("error of saving file!")
                /* error handling here */
            }

        }


    }
    /**
     Load saved game state if possible
     
     */
    func loadLevel(size: CGSize) -> Bool
    {
        //load score
        let defaults = NSUserDefaults.standardUserDefaults()
        currentScore = defaults.integerForKey(savedScoreName)
        //
        let file = "saved.txt" //file to read
        
        
        if let dir : NSString = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first
        {
      
            let path = dir.stringByAppendingPathComponent(file);
            //reading
            var loadText = NSString()
            do
            {
                 loadText = try NSString(contentsOfFile: path, encoding: NSUTF8StringEncoding)
            }
            catch
            {/* error handling here */
                print("error of reading file")
                return false
            }
            let result = String(loadText)
            let stringsArray =  result.characters.split {$0 == "\n"}.map { String($0) }
          
            SpritesManager.sharedManager.resetArray()
            
            for line in stringsArray
            {
               // print(line)
                let elemsChars = line.characters.split {$0 == " "}.map { String($0) }
                let className = elemsChars[0]
                let colNumber = elemsChars[1]
                let rowNumber = elemsChars[2]
                var optionalValue = "" // i.e. type of usual element or scissors direction
                
                let element = Element.createWithType(className, size: size)
                element.rowIndex = Int(rowNumber)!
                element.colIndex = Int(colNumber)!
                
                if className == "UsualElement"
                {
                    
                   let elem = element as! UsualElement
                   optionalValue = elemsChars[3]
                   elem.type = ElementType(rawValue: Int(optionalValue)!)!
                   SpritesManager.sharedManager.gameBoard[element.rowIndex][element.colIndex] = element
                    
                }
                else
                
                if className == "ScissorsElement"
                {
                    
                    let elem = element as! ScissorsElement
                    optionalValue = elemsChars[3]
                    elem.direction =  DirectionType(rawValue: Int(optionalValue)!)!
                    SpritesManager.sharedManager.gameBoard[element.rowIndex][element.colIndex] = element
                    
                }
                else
                {
                    SpritesManager.sharedManager.gameBoard[element.rowIndex][element.colIndex] = element
                }
                
            }
        }
        SpritesManager.sharedManager.printElem()
        return true
    }
}
