//
//  ChangeScissorsElement.swift
//  BlockSpriteSwiftGame
//
//  Created by Admin on 1/21/16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import SpriteKit
class ChangeScissorsElement: Element {
    
    override func onActivation() {
        
    }
    override func onDestroy()
    {
        
    }
    init(size: CGSize, color: UIColor = UIColor.whiteColor())
    {
        let texture = SKTexture(imageNamed: "changescissors")
        super.init(texture: texture, color: color, size: size)
        self.color = color
        self.size = size
    }
    init (element: ChangeScissorsElement)
    {
        let texture = SKTexture(imageNamed: "changescissors")
        super.init(texture: texture, color: element.color, size: element.size)
        self.color = element.color
        self.size = element.size
        self.rowIndex = element.rowIndex
        self.colIndex = element.colIndex
    }
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
}
