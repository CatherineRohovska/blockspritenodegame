//
//  FirstTryButtonNode.swift
//  BlockSpriteSwiftGame
//
//  Created by Admin on 2/9/16.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation
import SpriteKit

class FTButtonNode: SKSpriteNode {
    
    enum FTButtonActionType: Int {
        case TouchUpInside = 1,
        TouchDown, TouchUp
    }
    
    var isEnabled: Bool = true {
        didSet
        {
            if (disabledTexture != nil)
            {
                texture = isEnabled ? defaultTexture : disabledTexture
            }
        }
    }
    var isSelected: Bool = false
        {
        didSet
        {
            texture = isSelected ? selectedTexture : defaultTexture
        }
    }
    var defaultTexture: SKTexture
    var selectedTexture: SKTexture
    
    var textLabel: SKLabelNode
    required init(coder: NSCoder)
    {
        fatalError("NSCoding not supported")
    }
    
    init(normalTexture defaultTexture: SKTexture!, selectedTexture:SKTexture!, disabledTexture: SKTexture?)
    {
        
        self.defaultTexture = defaultTexture
        self.selectedTexture = selectedTexture
        self.disabledTexture = disabledTexture
        textLabel = SKLabelNode(fontNamed: "Chalkduster")
        super.init(texture: defaultTexture, color: UIColor.whiteColor(), size: defaultTexture.size())
        textLabel.zPosition = self.zPosition+1
        textLabel.fontSize = self.size.height/2
        textLabel.fontColor = UIColor.redColor()
        textLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
       // textLabel.frame.size = CGSize(width: self.frame.size.width, height: self.frame.size.height)
        userInteractionEnabled = true
        
        // Adding this node as an empty layer. Without it the touch functions are not being called
        let innerLayerNode = SKSpriteNode(texture: nil, color: UIColor.clearColor(), size: defaultTexture.size())
        innerLayerNode.position = self.position
        addChild(innerLayerNode)
        addChild(textLabel)
    }
    
    /**
     * Taking a target object and adding an action that is triggered by a button event.
     */
    func setButtonAction(target: AnyObject, triggerEvent event:FTButtonActionType, action:Selector)
    {
        
        switch (event) {
        case .TouchUpInside:
            targetTouchUpInside = target
            actionTouchUpInside = action
        case .TouchDown:
            targetTouchDown = target
            actionTouchDown = action
        case .TouchUp:
            targetTouchUp = target
            actionTouchUp = action
        }
        
    }
    
    var disabledTexture: SKTexture?
    var actionTouchUpInside: Selector?
    var actionTouchUp: Selector?
    var actionTouchDown: Selector?
    weak var targetTouchUpInside: AnyObject?
    weak var targetTouchUp: AnyObject?
    weak var targetTouchDown: AnyObject?
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        let touch: AnyObject! = touches.first!
        let touchLocation = touch.locationInNode(parent!)
        
        if (!isEnabled)
        {
            return
        }
        isSelected = true
        if (targetTouchDown != nil && targetTouchDown!.respondsToSelector(actionTouchDown!))
        {
            UIApplication.sharedApplication().sendAction(actionTouchDown!, to: targetTouchDown, from: self, forEvent: nil)
        }
        
        
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        
        if (!isEnabled)
        {
            return
        }
        
        let touch: AnyObject! = touches.first!
        let touchLocation = touch.locationInNode(parent!)
        
        if (CGRectContainsPoint(frame, touchLocation))
        {
            isSelected = true
        } else {
            isSelected = false
        }
        
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        
        if (!isEnabled)
        {
            return
        }
        
        isSelected = false
        
        if (targetTouchUpInside != nil && targetTouchUpInside!.respondsToSelector(actionTouchUpInside!))
        {
            let touch: AnyObject! = touches.first!
            let touchLocation = touch.locationInNode(parent!)
            
            if (CGRectContainsPoint(frame, touchLocation) )
            {
                UIApplication.sharedApplication().sendAction(actionTouchUpInside!, to: targetTouchUpInside, from: self, forEvent: nil)
            }
            
        }
        
        if (targetTouchUp != nil && targetTouchUp!.respondsToSelector(actionTouchUp!))
        {
            UIApplication.sharedApplication().sendAction(actionTouchUp!, to: targetTouchUp, from: self, forEvent: nil)
        }
    }
    
}