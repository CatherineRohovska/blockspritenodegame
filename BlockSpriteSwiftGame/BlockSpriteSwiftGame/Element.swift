//
//  Element.swift
//  BlockSpriteSwiftGame
//
//  Created by Admin on 1/20/16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import SpriteKit

enum AnimationAtlas: String{
    case scissorsUp = "scissorsUpAnimation"
    case scissorsDown = "scissorsDownAnimation"
    case scissorsRight = "scissorsRightAnimation"
    case scissorsLeft = "scissorsLeftAnimation"
    case changeType = "mystery"
    case gem = "gemAnimation"
    case remove = "removeAnimation"
}
class Element: SKSpriteNode {
   
  
    var rowIndex: Int = 0
    var colIndex: Int = 0
    var removeAnimation = Array<SKTexture>()
    /**
     Create and run texture animation from atlas name
     
     @param atlasName the name of atlas
     
     */
    func createTextureAnimationAndRun(atlasName: String)
    {
        let atlas = SKTextureAtlas(named: atlasName)
        var texturesArray = Array<SKTexture>()
        for textureName in atlas.textureNames
        {
            let texture = atlas.textureNamed(textureName)
            
            texturesArray.append(texture)
        }
        self.removeActionForKey("infinity")
        self.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(texturesArray, timePerFrame: 0.2)), withKey: "infinity")
        
    }
func onActivation()
{
    
}
func onDestroy()
{
    
}
    override  init(texture: SKTexture?, color: UIColor, size: CGSize)
    {
        
        super.init(texture: texture, color: color, size: size)
        let atlas = SKTextureAtlas(named: AnimationAtlas.remove.rawValue)
        for textureName in atlas.textureNames
        {
            let texture = atlas.textureNamed(textureName)
            removeAnimation.append(texture)
        }

    }
//
 
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
 static func createWithType(typeName: String, size: CGSize) -> Element
    {
        switch typeName {
        case "GemElement":
            return GemElement.init(size: size)
        case "UnbreakableElement":
            return UnbreakableElement.init(size: size)
        case "ChangeScissorsElement":
           return ChangeScissorsElement.init(size: size)
        case "ChangeTypeElement":
            return ChangeTypeElement.init(size: size)
        case "ScissorsElement":
            return ScissorsElement.init(size: size)
        case "BombElement":
            return BombElement.init(size: size)
        case "UsualElement":
           return UsualElement.init(size: size)
        default:
              return Element()
        }

    }
    
    static func createWithElement(element: Element) -> Element
    {
        if let temp = element as? GemElement
        {
            return GemElement(element: temp)
        }
        else
        if let temp = element as? UnbreakableElement
        {
            return UnbreakableElement(element: temp)
        }
        else
        if let temp = element as? ChangeScissorsElement
        {
            return ChangeScissorsElement(element: temp)
        }
        else
        if let temp = element as? ChangeTypeElement
        {
           return ChangeTypeElement(element: temp)
        }
        else
        if let temp = element as? ScissorsElement
        {
          return ScissorsElement(element: temp)
        }
        else
        if let temp = element as? BombElement
        {
          return BombElement(element: temp)
        }
        else
        if let temp = element as? UsualElement
        {
          return UsualElement(element: temp)
        }
        else
        {
        return Element()
        }
        
    }
}
