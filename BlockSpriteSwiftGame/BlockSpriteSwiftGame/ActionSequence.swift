//
//  ActionSequence.swift
//  BlockSpriteSwiftGame
//
//  Created by Admin on 1/22/16.
//  Copyright © 2016 Admin. All rights reserved.
//
/**
@header
Class describes sequence of actions or other sequences that could be executed in parallel or sequence
*/
import UIKit
import SpriteKit
enum ActionSync{
    case Seria
    case Parallel
}
enum Delays: Int{
    case move = 1
    case destroy = 0
}
class ActionSequence  {
    var actionsArray: Array<Action>
    var sequenceArray: Array<ActionSequence>
    var sync: ActionSync
    var duration = 0.0
    

    init()
    {
        sync = .Seria
        actionsArray = Array<Action>()
        sequenceArray = Array<ActionSequence>()
    }
    /**
     Run sequences contained in ActionSequence one by one
     
     @param array the array of ActionSequences
     
     @param index the index of element that must be processed
     
     @param completion the completion block executed at the end of processing
     
     */
    func runSequencesConsequently(array: Array<ActionSequence>, index: Int, completion: () -> Void = {})
    {
       let sequence = array[index]
        
            if sequence.sync == .Parallel
            {
                if sequence.sequenceArray.count>0
                {
                    runSequencesParalell(sequence.sequenceArray,  completion:
                    {
                        if index+1 < array.count
                        {
                            self.runSequencesConsequently(array, index: index+1, completion: completion)
                        }
                        else
                        {
                            completion()
                            
                        }
                        
                    }) //end of function parallel call
                }
                else
                {
                    runActionsParalell(sequence.actionsArray,  completion:
                    {
                        if index+1 < array.count
                        {
                            self.runSequencesConsequently(array, index: index+1, completion: completion)
                        }
                        else
                        {
                            completion()
                            
                        }
                            
                    }) //end of function parallel call
                }
            }
            else
            {
                if sequence.sequenceArray.count>0
                {
                    runSequencesConsequently(sequence.sequenceArray, index: 0, completion:
                    {
                        if index+1 < array.count
                        {
                            self.runSequencesConsequently(array, index: index+1, completion: completion)
                        }
                        else
                        {
                           completion()
                            
                        }
                    }) //end of function consequently call
                }
                else
                {
                    runActionsConsequently(sequence.actionsArray, index: 0, completion:
                    {
                            if index+1 < array.count
                            {
                                self.runSequencesConsequently(array, index: index+1, completion: completion)
                            }
                            else
                            {
                                completion()
                                
                            }
                    }) //end of function consequently call
                }
            }
        
    
    }
    /**
     Run actions contained in ActionSequence one by one
     
     @param array the array of Actions
     
     @param index the index of element that must be processed
     
     @param completion the completion block executed at the end of processing
     
     */
    func runActionsConsequently(array:Array<Action>, index: Int, completion: () -> Void = {})
    {
        let action = array[index]
        if index < array.count-1
        {
            action.element.runAction(action.createActionForElement(), completion:
                {
                    action.finalize()
                    self.runActionsConsequently(array, index: index+1, completion:  completion)
                })
        }
        else
        {
            action.element.runAction(action.createActionForElement(),completion:
                {
                action.finalize()
                print("action in sequence take completion", action.type, " element ", SpritesManager.sharedManager.typeOfElement(action.element))
                completion()
                })
        }
        
    }
    /**
     Run sequences contained in ActionSequence in parallel
     
     @param array the array of ActionSequences
     
     @param index the index of element that must be processed
     
     @param completion the completion block executed at the end of processing
     
     */
    func runSequencesParalell(array:Array<ActionSequence>, completion: () -> Void = {})
    {
        var indexOfMaxDurationSequence = array.count-1
        var maxDurationSequence = 0.0
        for (index, sequence) in array.enumerate()
        {
            if sequence.getDuration() > maxDurationSequence
            {
                maxDurationSequence = sequence.duration
                indexOfMaxDurationSequence = index
            }
        }
        for (index, sequence) in array.enumerate()
        {
            
            if sequence.sync == .Seria //execute consequently
            {
                if index == indexOfMaxDurationSequence
                {
                    if sequence.sequenceArray.count>0
                    {
                        runSequencesConsequently(sequence.sequenceArray, index: 0, completion: completion)
                    }
                    else
                    {
                        runActionsConsequently(sequence.actionsArray, index: 0, completion: completion)
                    }
                    
                }
                else
                {
                    if sequence.sequenceArray.count>0
                    {
                        runSequencesConsequently(sequence.sequenceArray, index: 0)
                    }
                    else
                    {
                        runActionsConsequently(sequence.actionsArray, index: 0)
                    }
                }
                
            }
            else //execute parallel
            {
                if index == array.count-1
                {
                    if sequence.sequenceArray.count>0
                    {
                        runSequencesParalell(sequence.sequenceArray, completion: completion)
                    }
                    else
                    {
                        runActionsParalell(sequence.actionsArray, completion: completion)
                    }
                    
                }
                else
                {
                    if sequence.sequenceArray.count>0
                    {
                        runSequencesParalell(sequence.sequenceArray)
                    }
                    else
                    {
                        runActionsParalell(sequence.actionsArray)
                    }
                }
            }
        }
    }
    /**
     Run actions contained in ActionSequence in parallel
     
     @param array the array of Actions
     
     @param completion the completion block executed at the end of processing
     
     */
    func runActionsParalell(array:Array<Action>,  completion: () -> Void = {})
    {
        var longestDurationIndex: Int = 0
        var longestDuration = -0.5
       // var isDurationEquial = false
        var countOfEquialDurations = 0
        for (index, action) in array.enumerate()
        {
            if action.duration >= longestDuration
            {
                longestDurationIndex = index
                longestDuration = action.duration
            }
//            if action.duration == longestDuration
//            {
//                countOfEquialDurations++
//            }
            
        }
        //
        for action in array
        {
            if action.duration == longestDuration
            {
                countOfEquialDurations++
            }
            
        }
        //
        if  countOfEquialDurations != array.count
        {
            for (index, action) in array.enumerate()
            {
            
                if index == longestDurationIndex
                {
                    action.element.runAction(action.createActionForElement(), completion:{
                    
                        action.finalize();
                 
                        completion()
                    })
                
                }
                else
                {
                    action.element.runAction(action.createActionForElement(), completion: {
                    
                        action.finalize()
                        
                    })
                }
            }
        }
        else
        {
            for  action in array
            {
                action.element.runAction(action.createActionForElement(), completion:{
                    action.finalize()
                    var countOfFinalized = 0
                    for  actionPart in array
                    {
                        if actionPart.finalized
                        {
                            countOfFinalized++
                    
                        }
                    }
                        if countOfFinalized == array.count
                        {
                            completion()
                        }
                })
            }
        }
    }
    /**
     Run process of parcing and execution of actions contained in ActionSequence
     
     @param completion the completion block executed at the end of processing
     
     */
    func start(completion: () -> Void)
    {
        if actionsArray.count > 0
        {
            if sync == .Seria
            {
                runActionsConsequently(actionsArray, index: 0, completion: completion)
            }
            else
            {
                runActionsParalell(actionsArray, completion: completion)
            }
            
        }
        else
            if sequenceArray.count > 0
            {
                if sync == .Seria
                {
                    runSequencesConsequently(sequenceArray,index: 0, completion: completion)
                }
                else
                {
                    runSequencesParalell(sequenceArray, completion: completion)
                }
            
            }
    }
    /**
     Calculates the duration of whole ActionSequence
     
     
     @return whole duration of array of actions or sequences
     
     */
    func getDuration() -> Double
    {
        var duration1 = 0.0;
        if sequenceArray.count != 0
        {
            if sync == .Parallel
            {
                for seq in sequenceArray
                {
                    let seqDuration = seq.getDuration()
                    if seqDuration >= duration1
                    {
                        duration1 = seqDuration
                    }
                }
            }
            else
            {
                for seq in sequenceArray
                {
                    let seqDuration = seq.getDuration()
                    duration1 += seqDuration
                    
                }
                
            }
            
        }
        else
        {
            if actionsArray.count != 0
            {
                if sync == .Parallel
                {
                    for act in actionsArray
                    {
                        if act.duration >= duration
                        {
                            duration1 = act.duration
                        }
                    }
                }
                else
                {
                    for act in actionsArray
                    {
                        duration1 += act.duration
                    }
                }
                
            }
        }
        
        duration = duration1
        return duration1
    }
  
 
}
