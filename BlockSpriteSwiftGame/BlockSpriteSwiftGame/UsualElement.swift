//
//  UsualElement.swift
//  BlockSpriteSwiftGame
//
//  Created by Admin on 1/21/16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import SpriteKit
enum ElementType: Int
{
    case zero = 0
    case first = 1
    case second = 2
    case third = 3
    case fourth = 4
    case fifth = 5
    
}
class UsualElement: Element {
    var isSelected = false{
        didSet{
            switch isSelected
            {
            case true:
            self.texture =  SKTexture(imageNamed: touchedTexture)
            default:
             self.texture =  SKTexture(imageNamed: freeTexture)
            }
            
        }
    }
    var freeTexture = ""
    var touchedTexture = ""
    var type: ElementType =  ElementType(rawValue: 0)!
    {
        didSet
        {
            switch type.rawValue
            {
            case 6:
                self.texture =  SKTexture(imageNamed: "usual5")
                freeTexture = "usual5"
                touchedTexture = "usual5selected"
            case 1:
                self.texture =  SKTexture(imageNamed: "usual1")
                freeTexture = "usual1"
                touchedTexture = "usual1selected"//self.color = UIColor.redColor()
            case 2:
                self.texture =  SKTexture(imageNamed: "usual2")
                freeTexture = "usual2"
                touchedTexture = "usual2selected"//self.color = UIColor.greenColor()
            case 3:
                self.texture =  SKTexture(imageNamed: "usual3")
                freeTexture = "usual3"
                touchedTexture = "usual3selected"// self.color = UIColor.blueColor()
            case 5:
                self.texture =  SKTexture(imageNamed: "usual0")
                freeTexture = "usual0"
                touchedTexture = "usual0selected"
            default:
                self.texture =  SKTexture(imageNamed: "usual4")
                freeTexture = "usual4"
                touchedTexture = "usual4selected"// self.color = color
            }
        }
            
    }
    override func onActivation() {
        if isSelected
        {
            print(touchedTexture)
            isSelected = false
        }
        else
        {
            isSelected = true
        }
    }
    override func onDestroy()
    {
        
    }
    init(size: CGSize, color: UIColor = UIColor.whiteColor())
    {
        let texture = SKTexture(imageNamed: "usual")
        super.init(texture: texture, color: color, size: size)
        self.color = color
        self.size = size
        self.type = ElementType(rawValue: Int(arc4random_uniform(5) + 1))!
        switch type.rawValue
        {
        case 6:
            self.texture =  SKTexture(imageNamed: "usual5")
            freeTexture = "usual5"
            touchedTexture = "usual5selected"
        case 1:
            self.texture =  SKTexture(imageNamed: "usual1")
            freeTexture = "usual1"
            touchedTexture = "usual1selected"//self.color = UIColor.redColor()
        case 2:
            self.texture =  SKTexture(imageNamed: "usual2")
            freeTexture = "usual2"
            touchedTexture = "usual2selected"//self.color = UIColor.greenColor()
        case 3:
            self.texture =  SKTexture(imageNamed: "usual3")
            freeTexture = "usual3"
            touchedTexture = "usual3selected"// self.color = UIColor.blueColor()
        case 5:
            self.texture =  SKTexture(imageNamed: "usual0")
            freeTexture = "usual0"
            touchedTexture = "usual0selected"
        default:
            self.texture =  SKTexture(imageNamed: "usual4")
            freeTexture = "usual4"
            touchedTexture = "usual4selected"// self.color = color
        }
    }
    init(element: UsualElement)
    {
        let texture = SKTexture(imageNamed: "usual")
        super.init(texture: texture, color: element.color, size: element.size)
        self.colorBlendFactor = 0.5
        self.color = element.color
        self.size = element.size
        self.type = element.type
        self.rowIndex = element.rowIndex
        self.colIndex = element.colIndex
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
