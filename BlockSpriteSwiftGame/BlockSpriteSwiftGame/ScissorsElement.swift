//
//  ScissorsElement.swift
//  BlockSpriteSwiftGame
//
//  Created by Admin on 1/21/16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import SpriteKit
enum DirectionType: Int
{
    case left = 1
    case down = 2
    case up = 3
    case right = 4
}
class ScissorsElement: Element {
    var atlasNameForMovingTextures = AnimationAtlas.scissorsRight.rawValue
    var direction = DirectionType(rawValue: 4)!
    {
        didSet
        {
                switch direction.rawValue
                {
                case 1:
                    self.texture = SKTexture(imageNamed: "scissorsleft")
                    atlasNameForMovingTextures = AnimationAtlas.scissorsLeft.rawValue
                case 2:
                    self.texture = SKTexture(imageNamed: "scissorsdown")
                    atlasNameForMovingTextures = AnimationAtlas.scissorsDown.rawValue
                case 3:
                    self.texture = SKTexture(imageNamed: "scissorsup")
                    atlasNameForMovingTextures = AnimationAtlas.scissorsUp.rawValue
                default:
                    self.texture = SKTexture(imageNamed: "scissorsright")
                    atlasNameForMovingTextures = AnimationAtlas.scissorsRight.rawValue
                }
        }
        
    }
    override func onActivation() {
        
    }
    override func onDestroy()
    {
        
    }
    
    init(size: CGSize, color: UIColor = UIColor.whiteColor())
    {
        var texture = SKTexture(imageNamed: "usual")
        self.direction = DirectionType(rawValue:  Int(arc4random_uniform(4) + 1))!
        switch direction
        {
        case .left:
            texture = SKTexture(imageNamed: "scissorsleft")
            atlasNameForMovingTextures = AnimationAtlas.scissorsLeft.rawValue
        case .down:
            texture = SKTexture(imageNamed: "scissorsdown")
            atlasNameForMovingTextures = AnimationAtlas.scissorsDown.rawValue
        case .up:
            texture = SKTexture(imageNamed: "scissorsup")
            atlasNameForMovingTextures = AnimationAtlas.scissorsUp.rawValue
        default:
            texture = SKTexture(imageNamed: "scissorsright")
            atlasNameForMovingTextures = AnimationAtlas.scissorsRight.rawValue
        }
        super.init(texture: texture, color: color, size: size)
        self.color = color
        self.size = size
     
    }
    init (element: ScissorsElement)
    {
        var texture = SKTexture(imageNamed: "usual")
        self.direction = element.direction
        switch direction
        {
        case .left:
            texture = SKTexture(imageNamed: "scissorsleft")
            atlasNameForMovingTextures = AnimationAtlas.scissorsLeft.rawValue
        case .down:
            texture = SKTexture(imageNamed: "scissorsdown")
            atlasNameForMovingTextures = AnimationAtlas.scissorsDown.rawValue
        case .up:
            texture = SKTexture(imageNamed: "scissorsup")
            atlasNameForMovingTextures = AnimationAtlas.scissorsUp.rawValue
        default:
            texture = SKTexture(imageNamed: "scissorsright")
            atlasNameForMovingTextures = AnimationAtlas.scissorsRight.rawValue
        }
        super.init(texture: texture, color: element.color, size: element.size)
        self.color = element.color
        self.size = element.size
        self.rowIndex = element.rowIndex
        self.colIndex = element.colIndex
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
}
