//
//  GemElement.swift
//  BlockSpriteSwiftGame
//
//  Created by Admin on 1/21/16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import SpriteKit
class GemElement: Element {
    override func onActivation()
    {
        
    }
 
    init( size: CGSize, color: UIColor = UIColor.whiteColor())
    {
     //super.init()
    let texture = SKTexture(imageNamed: "gem")
     super.init(texture: texture, color: color, size: size)
     self.color = color
     self.size = size
        super.createTextureAnimationAndRun(AnimationAtlas.gem.rawValue)
     
    }
    init (element: GemElement)
    {
        let texture = SKTexture(imageNamed: "gem")
        super.init(texture: texture, color: element.color, size: element.size)
        self.color = element.color
        self.size = element.size
        self.rowIndex = element.rowIndex
        self.colIndex = element.colIndex
        super.createTextureAnimationAndRun(AnimationAtlas.gem.rawValue)
    }

    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
   
}
