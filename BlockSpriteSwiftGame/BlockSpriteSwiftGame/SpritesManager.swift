//
//  SpritesManager.swift
//  BlockSpriteSwiftGame
//
//  Created by Admin on 1/21/16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import SpriteKit
class SpritesManager {
    static let sharedManager = SpritesManager()
    private var startingElement: UsualElement?
    let numRows = 7
    let numCols = 8
    var gameBoard =  Array<Array<Element>>()
    var startType: ElementType = ElementType.zero
    var countOfTheSame: Int
    private  init()
    {
        for _ in 0...numRows
        {
            gameBoard.append(Array(count:numCols, repeatedValue:Element()))
        }
        countOfTheSame = 0
        startingElement = nil
    }
    
    /**
     Add new row to place it on the scene
     
     @param size the size of generated sprite
     
     @param isGems the flag indicates that whole row will be created with only gems (optional)
     
     @param elemArray the array of elements which needs to be duplicated (optional)
     
     @return (elements, actions) pair of array & ActionSequence created from sprites and actions attached to it
     
     */
    
    func addRow(size: CGSize, isGems: Bool = false, elemArray: Array<Element> = Array<Element>())->(elements: Array<Element>, actions: ActionSequence)
    {
        var array = Array<Element>()
        let action = ActionSequence()
        for index in 0...numCols-1
        {
            var str: String
            if !isGems
            {
                str = generateRandomClass()
                if index == 0
                {
                    str = "ChangeTypeElement"
                }
                if index == 2
                {
                    str = "BombElement"
                }
                if index == 4
                {
                    str = "ScissorsElement"
                }
                if index == 3
                {
                    str = "UnbreakableElement"
                }
                if index == 5
                {
                    str = "ChangeScissorsElement"
                }
            }
            else
            {
                str = "GemElement"
            }
            print(str)
            if elemArray.count==0
            {
            array.append(Element.createWithType(str, size: size))
            }
            else
            {
                array.append(Element.createWithElement(elemArray[index]))
            }
        
            array[index].colIndex = index
            array[index].rowIndex = 0;
            let sAction = Action()
            sAction.element = array[index]
            sAction.type = ActionType.Move
            sAction.nextPosition.colIndex = index
            if (getLastEmptyNode(index) != -1) //if we have route to place --
            {
                sAction.nextPosition.rowIndex = getLastEmptyNode(index)
                gameBoard[getLastEmptyNode(index)][index] = array[index]
            }
            else
            {
                array = Array<Element>()
                let resultSeq = ActionSequence()
                return (array,resultSeq)
                //placeholder for actions to lose?
            }
            action.actionsArray.append(sAction)
            action.sync = ActionSync.Parallel
        }
        let resultSeq = ActionSequence()
        resultSeq.sequenceArray.append(action)
      
        return (array, resultSeq)
    }
    /**
     Generate name of created element class
     
     @param array the array of Actions
     
     @return name of class that must be generated
     
     */
    private func generateRandomClass() ->String
    {
       // return "UsualElement"
        let indicator = Int(arc4random_uniform(15) + 2) // we don't need random gems so start from 2
        switch indicator {
        case 1:
            return "GemElement"
        case 2:
            return "UnbreakableElement"
        case 3:
            return "ChangeScissorsElement"
        case 4:
            return "ChangeTypeElement"
        case 5:
            return "ScissorsElement"
        case 6:
            return "BombElement"
        default:
            return "UsualElement"
        }
    }
    /**
     Action on bomb activation
     
     @param iPos row position of element in array
     
     @param jPos column position of element in array
     
     @return ActionSequence of deletion certain elements
     */
    func bombActivate(positionInRow: Int, positionInColumn: Int) -> ActionSequence
    {
        let nextRowPosition = positionInRow+1
        let previousRowPosition = positionInRow-1
        let nextColumnPosition = positionInColumn+1
        let previousColumnPosition = positionInColumn-1
        let action = ActionSequence()
        
        let nextIExists = 0...numRows-1 ~= nextRowPosition
        let nextJExists = 0...numCols-1 ~= nextColumnPosition
        let prevIExists = 0...numRows-1 ~= previousRowPosition
        let prevJExists = 0...numCols-1 ~= previousColumnPosition
        
        let act = Action()
        act.type = .Destroy
        act.element = gameBoard[positionInRow][positionInColumn]
        action.actionsArray.append(act)
        
        gameBoard[positionInRow][positionInColumn] = Element()
        var actNeighbourElements = Array<Element>()
        //row below
        if (nextIExists)
        {
            if (checkNotEmptyElement(gameBoard[nextRowPosition][positionInColumn]) && gameBoard[nextRowPosition][positionInColumn] as? GemElement == nil)
            {
                let elem = gameBoard[nextRowPosition][positionInColumn]
                actNeighbourElements.append(elem)
                gameBoard[nextRowPosition][positionInColumn] = Element()
                
            }
            if (nextJExists)
            {
                if (checkNotEmptyElement(gameBoard[nextRowPosition][nextColumnPosition]) && gameBoard[nextRowPosition][nextColumnPosition] as? GemElement == nil)
                {
                    let elem = gameBoard[nextRowPosition][nextColumnPosition]
                    actNeighbourElements.append(elem)
                    gameBoard[nextRowPosition][nextColumnPosition] = Element()
                    
                }
            }
            if (prevJExists)
            {
                if (checkNotEmptyElement(gameBoard[nextRowPosition][previousColumnPosition]) && gameBoard[nextRowPosition][previousColumnPosition] as? GemElement == nil)
                {
                    let elem = gameBoard[nextRowPosition][previousColumnPosition]
                    actNeighbourElements.append(elem)
                    gameBoard[nextRowPosition][previousColumnPosition] = Element()
                    
                }
            }
        }
        // middle row
        if (nextJExists)
        {
            if (checkNotEmptyElement(gameBoard[positionInRow][nextColumnPosition]) && gameBoard[positionInRow][nextColumnPosition] as? GemElement == nil)
            {
                let elem = gameBoard[positionInRow][nextColumnPosition]
                actNeighbourElements.append(elem)
                gameBoard[positionInRow][nextColumnPosition] = Element()
                
            }
            
        }
        if (prevJExists)
        {
            if (checkNotEmptyElement(gameBoard[positionInRow][previousColumnPosition]) && gameBoard[positionInRow][previousColumnPosition] as? GemElement == nil)
            {
                let elem = gameBoard[positionInRow][previousColumnPosition]
                actNeighbourElements.append(elem)
                gameBoard[positionInRow][previousColumnPosition] = Element()
                
            }
        }
        //row above
        if (prevIExists)
        {
            if (checkNotEmptyElement(gameBoard[previousRowPosition][positionInColumn]) && gameBoard[previousRowPosition][positionInColumn] as? GemElement == nil)
            {
                let elem = gameBoard[previousRowPosition][positionInColumn]
                actNeighbourElements.append(elem)
                gameBoard[previousRowPosition][positionInColumn] = Element()
                
            }
            if (nextJExists)
            {
                if (checkNotEmptyElement(gameBoard[previousRowPosition][nextColumnPosition]) && gameBoard[previousRowPosition][nextColumnPosition] as? GemElement == nil)
                {
                    let elem = gameBoard[previousRowPosition][nextColumnPosition]
                    actNeighbourElements.append(elem)
                    gameBoard[previousRowPosition][nextColumnPosition] = Element()
                    
                }
            }
            if (prevJExists)
            {
                if (checkNotEmptyElement(gameBoard[previousRowPosition][previousColumnPosition]) && gameBoard[previousRowPosition][previousColumnPosition] as? GemElement == nil  )
                {
                    let elem = gameBoard[previousRowPosition][previousColumnPosition]
                    actNeighbourElements.append(elem)
                    gameBoard[previousRowPosition][previousColumnPosition] = Element()
                    
                }
            }
        }
        // iterate each element and create actions
        for elem in actNeighbourElements
        {
            let act = Action()
            act.element = elem
            act.type = .Destroy
            action.actionsArray.append(act)
        }
        action.sync = .Parallel
        return action
    }
    /**
     Changes performed on scissors activation
     
     @param iPos row position of element in array
     
     @param jPos column position of element in array
     
     @return ActionSequence of deletion certain elements and moving scissors
     
     */
    func scissorsActivate (positionInRow: Int, positionInColumn: Int) -> ActionSequence
    {
        let action = ActionSequence()
        var endRowIndex = positionInRow
        var endColumnIndex = positionInColumn
        
        let scissors = gameBoard[positionInRow][positionInColumn] as! ScissorsElement
        //run animation of scissors depending on its direction
        scissors.createTextureAnimationAndRun(scissors.atlasNameForMovingTextures)
        //
        switch scissors.direction
        {
        case .up:
            endRowIndex = 0
            endColumnIndex = positionInColumn
        case .left:
            endRowIndex = positionInRow
            endColumnIndex = 0
        case .down:
            endRowIndex = numRows - 1
            endColumnIndex = positionInColumn
        default:
            endColumnIndex = numCols - 1
            endRowIndex = positionInRow
        }
        // if moving up and not last element
        if scissors.direction == .up
        {
            let scissorsSequence = ActionSequence()
            scissorsSequence.sync = .Seria
            
            let elemSequence = ActionSequence()
            elemSequence.sync = .Seria
            
            let scissorsAction = Action()
            
            
            gameBoard[positionInRow][positionInColumn] = Element()
            if  0...numRows-1 ~= positionInRow-1
            {
                //check on cristall existence
                for var indexInRow = endRowIndex; indexInRow<=positionInRow; indexInRow++
                {
                    if (gameBoard[indexInRow][positionInColumn] as? GemElement != nil)
                    {
                        endRowIndex = indexInRow+1
                    }
                }
                if (positionInRow-endRowIndex != 0) //if position may be changed
                {
                    scissorsAction.type = .Move
                    scissorsAction.element = scissors
                    scissorsAction.element.zPosition = 10
                    scissorsAction.nextPosition.colIndex = endColumnIndex
                    scissorsAction.nextPosition.rowIndex = endRowIndex
                
                    scissorsSequence.actionsArray.append(scissorsAction)
                }

                for var indexInRow = positionInRow-1; indexInRow>=endRowIndex; indexInRow--
                {
                    let elem = gameBoard[indexInRow][positionInColumn]
                    //if we meet empty element -- create delay for moving action processing
                    if checkEmptyElement(elem)
                    {
                        let actElem = Action()
                        actElem.element = scissors
                        actElem.type = .Pause
                        elemSequence.actionsArray.append(actElem)
                        
                    }
                    if (!checkEmptyElement(elem) &&  elem as? GemElement == nil)
                    {
                   
                        let actElem = Action()
                        actElem.element = elem
                        actElem.type = .Destroy
                        elemSequence.actionsArray.append(actElem)
                        gameBoard[indexInRow][positionInColumn] = Element()
                    }
             
                }
            }
            let actScissors = Action()
            actScissors.element = scissors
            actScissors.type = .Destroy
            scissorsSequence.actionsArray.append(actScissors)
            gameBoard[endRowIndex][positionInColumn] = Element()
            //
            action.sequenceArray.append(scissorsSequence)
            if (elemSequence.actionsArray.count != 0)
            {
                action.sequenceArray.append(elemSequence)
            }
        }
        // if moving down
    
        if scissors.direction == .down
        {
            let scissorsSequence = ActionSequence()
            scissorsSequence.sync = .Seria
            
            let elemSequence = ActionSequence()
            elemSequence.sync = .Seria
            
            let scissAct = Action()
           
            
            gameBoard[positionInRow][positionInColumn] = Element()
            if 0...numRows-1 ~= positionInRow+1 //if element not last
            {
                //check on cristall existence 
                for var indexInRow = endRowIndex; indexInRow>=positionInRow; indexInRow--
                {
                    if (gameBoard[indexInRow][positionInColumn] as? GemElement != nil)
                    {
                        endRowIndex = indexInRow-1
                    }
                }
                if (positionInRow-endRowIndex != 0) //if position may be changed
                {
                    scissAct.type = .Move
                    scissAct.element = scissors
                    scissAct.element.zPosition = 10
                    print("Next pos: ")
                    scissAct.nextPosition.colIndex = endColumnIndex
                    scissAct.nextPosition.rowIndex = endRowIndex
                
                    scissorsSequence.actionsArray.append(scissAct)
                }

                for var indexInRow = positionInRow+1; indexInRow<=endRowIndex; indexInRow++
                {
                    let elem = gameBoard[indexInRow][positionInColumn]
                    //if we meet empty element -- create delay for moving action processing
                    if checkEmptyElement(elem)
                    {
                        let actElem = Action()
                        actElem.element = scissors
                        actElem.type = .Pause
                        elemSequence.actionsArray.append(actElem)
                    }
          
                    if (!checkEmptyElement(elem) && elem as? GemElement == nil)
                    {
                        let actElem = Action()
                        actElem.element = elem
                        actElem.type = .Destroy
                        elemSequence.actionsArray.append(actElem)
                        gameBoard[indexInRow][positionInColumn] = Element()
                    }
                   
                
                }
            }
            let actScissors = Action()
            actScissors.element = scissors
            actScissors.type = .Destroy
            scissorsSequence.actionsArray.append(actScissors)
            gameBoard[endRowIndex][positionInColumn] = Element()
            action.sequenceArray.append(scissorsSequence)
            if (elemSequence.actionsArray.count != 0)
            {
                action.sequenceArray.append(elemSequence)
            }
            
        }
        // if moving left
        if scissors.direction == .left
        {
            let scissorsSequence = ActionSequence()
            scissorsSequence.sync = .Seria
            
            let elemSequence = ActionSequence()
            elemSequence.sync = .Seria
            
            let scissAct = Action()
       
            gameBoard[positionInRow][positionInColumn] = Element()
            if 0...numCols-1 ~= positionInColumn-1 //if element is not last
            {
                //check on cristall existence
                for var indexInColumn = endColumnIndex; indexInColumn<=positionInColumn; indexInColumn++
                {
                    if (gameBoard[positionInRow][indexInColumn] as? GemElement != nil)
                    {
                        endColumnIndex = indexInColumn+1
                    }
                }
                if (positionInColumn-endColumnIndex != 0) //if position may be changed
                {
                    scissAct.type = .Move
                    scissAct.element = scissors
                    scissAct.element.zPosition = 10
                    scissAct.nextPosition.colIndex = endColumnIndex
                    scissAct.nextPosition.rowIndex = endRowIndex
                
                    scissorsSequence.actionsArray.append(scissAct)
                }
   
                for var indexInColumn = positionInColumn-1; indexInColumn>=endColumnIndex; indexInColumn--
                {
              
                    let elem = gameBoard[positionInRow][indexInColumn]
                    //if we meet empty element -- create delay for moving action processing
                    if checkEmptyElement(elem)
                    {
                  
                        let actElem = Action()
                        actElem.element = scissors
                        actElem.type = .Pause
                        elemSequence.actionsArray.append(actElem)
                        
                    }

                    if (!checkEmptyElement(elem)  && elem as? GemElement == nil)
                    {
                        
                        let actElem = Action()
                        actElem.element = elem
                        actElem.type = .Destroy
                        elemSequence.actionsArray.append(actElem)
                        gameBoard[positionInRow][indexInColumn] = Element()
                    }
                 
                   
                }
            }
            let actScissors = Action()
            actScissors.element = scissors
            actScissors.type = .Destroy
            scissorsSequence.actionsArray.append(actScissors)
            gameBoard[positionInRow][endColumnIndex] = Element()
            action.sequenceArray.append(scissorsSequence)
            if (elemSequence.actionsArray.count != 0)
            {
                action.sequenceArray.append(elemSequence)
            }

            
        }
        
        //if moving right
        if scissors.direction == .right
        {
            let scissorsSequence = ActionSequence()
            scissorsSequence.sync = .Seria
            
            let elemSequence = ActionSequence()
            elemSequence.sync = .Seria
            
            let scissAct = Action()
            
            gameBoard[positionInRow][positionInColumn] = Element()
            if 0...numCols-1 ~= positionInColumn+1 //if element is not last
            {
                //check on cristall existence
                for var indexInColumn = positionInColumn; indexInColumn<=endColumnIndex; indexInColumn++
                {
                    if (gameBoard[positionInRow][indexInColumn] as? GemElement != nil)
                    {
                        endColumnIndex = indexInColumn-1
                    }
                }
                if (positionInColumn-endColumnIndex != 0) //if position may be changed
                {
                    scissAct.type = .Move
                    scissAct.element = scissors
                    scissAct.element.zPosition = 10
                    scissAct.nextPosition.colIndex = endColumnIndex
                    scissAct.nextPosition.rowIndex = endRowIndex
                
                    scissorsSequence.actionsArray.append(scissAct)
                }

                for var indexInColumn = positionInColumn+1; indexInColumn<=endColumnIndex; indexInColumn++
                {
                
                    let elem = gameBoard[positionInRow][indexInColumn]
                    //if we meet empty element -- create delay for moving action processing
                    if checkEmptyElement(elem)
                    {

                        let actElem = Action()
                        actElem.element = scissors
                        actElem.type = .Pause
                        elemSequence.actionsArray.append(actElem)
                        
                    }
                    if (!checkEmptyElement(elem)  && elem as? GemElement == nil)
                    {
                        
                        let actElem = Action()
                        actElem.element = elem
                        actElem.type = .Destroy
                        elemSequence.actionsArray.append(actElem)
                        gameBoard[positionInRow][indexInColumn] = Element()
                    }
                   
                }
            }
            let actScissors = Action()
            actScissors.element = scissors
            actScissors.type = .Destroy
            scissorsSequence.actionsArray.append(actScissors)
            gameBoard[positionInRow][endColumnIndex] = Element()
            action.sequenceArray.append(scissorsSequence)
            if (elemSequence.actionsArray.count != 0)
            {
                action.sequenceArray.append(elemSequence)
            }

        }
        
        action.sync = .Parallel
        return action
        
    }
    
    /**
     Check group of usual elements of certain type and prepare this group for deletion
     
     @param iPos row position of element in array
     
     @param jPos column position of element in array
     
   
     */
    func checkElems(positionInRow: Int, positionInColumn: Int)
    {
        let elem = gameBoard[positionInRow][positionInColumn] as? UsualElement

        if (elem  != nil && elem?.type != .zero )
        {
            if ((startingElement == nil))
            {
                startingElement = elem
                startType = elem!.type
            }
            
           
            //            ***
            //            *0+
            //            ***
            if (0...numRows-1 ~= positionInRow+1)
            {
                let compare = gameBoard[positionInRow+1][positionInColumn] as? UsualElement
                if(compare?.type == startType)
                {
                    countOfTheSame++
                    elem!.type = .zero
                    checkElems(positionInRow+1, positionInColumn: positionInColumn)
                    
                }
            }
            //            ***
            //            +0*
            //            ***
            if (0...numRows-1 ~= positionInRow-1)
            {
                let compare = gameBoard[positionInRow-1][positionInColumn] as? UsualElement
                if (compare?.type == startType)
                {
                    countOfTheSame++
                    elem!.type = .zero
                    checkElems(positionInRow-1, positionInColumn: positionInColumn)
                }
            }
            //            *+*
            //            *0*
            //            ***
            if (0...numCols-1 ~= positionInColumn-1)
            {
                let compare = gameBoard[positionInRow][positionInColumn-1] as? UsualElement
                if (compare?.type == startType)
                {
                    countOfTheSame++
                    elem!.type = .zero
                    checkElems(positionInRow, positionInColumn: positionInColumn-1)
                }
            }
            //            ***
            //            *0*
            //            *+*
            if (0...numCols-1 ~= positionInColumn+1)
            {
                let compare = gameBoard[positionInRow][positionInColumn+1] as? UsualElement
                if (compare?.type == startType)
                {
                    countOfTheSame++
                    elem!.type = .zero
                    checkElems(positionInRow, positionInColumn: positionInColumn+1)
                }
                
            }
            //
            if(countOfTheSame>0)
            {
                elem!.type = .zero
            }
            //
            if ((startingElement == elem))
            {
                countOfTheSame++
                print(countOfTheSame)
                countOfTheSame=0
                startingElement = nil
       
            }
        }
        
    }
    /**
     Changes performed on deletion usual elements of "zero" type
     
     
     @return ActionSequence of deletion certain elements
     
     */
  func createDeleteActions() -> ActionSequence
  {
    let action = ActionSequence()
    for indexInRow in 0...numRows-1
    {
        for indexInColumn in 0...numCols-1
        {
            if let elem = gameBoard[indexInRow][indexInColumn] as? UsualElement
            {
                if elem.type == .zero
                {
                    let act = Action()
                    // change array
                    gameBoard[indexInRow][indexInColumn] = Element()
                    //
                    act.element = elem
                    act.type = .Destroy
                    action.actionsArray.append(act)
                    //
                    
                }
            }
        }
    }
    action.sync = .Parallel
    return action
  }
    /**
     Changes performed on scissors activation
     
     @param col the number of column in which calculates last avaliable position
     
     @return number of empty element in column row
     
     */
    func getLastEmptyNode(inColumn: Int) -> Int
    {
        for var indexInRow = numRows-1; indexInRow>=0; indexInRow--
        {
            if(checkEmptyElement(gameBoard [indexInRow][inColumn]))
            {
                return indexInRow
                
            }
            
        }
        return -1
    }
    /**
     Check if elements on board can be moved down
     
     @return ActionSequence of moving elements
     
     */
    func createDownActions() -> ActionSequence
    {
        let action = ActionSequence()
        var numberOfStepsDown: Int = 0
        for indexOfColumn in 0...numCols-1
        {
            var isGemFallsOut = false
            for var indexOfRow = numRows-1; indexOfRow>=0; indexOfRow--
            {
                if  (checkEmptyElement(gameBoard [indexOfRow][indexOfColumn]))
                {
                    numberOfStepsDown++
                }
                else
                if ( numberOfStepsDown != 0)
                {
                    let elem = gameBoard [indexOfRow][indexOfColumn]
                    let act = Action()
                    act.element = elem
                    act.type = .Move
                    act.nextPosition.colIndex = indexOfColumn
                    
                    if (indexOfRow+numberOfStepsDown < numRows)
                    {
               
                        act.nextPosition.rowIndex = indexOfRow+numberOfStepsDown
              
                        gameBoard [indexOfRow+numberOfStepsDown][indexOfColumn] = elem
                        gameBoard [indexOfRow][indexOfColumn] = Element()
                        //if we got rid of gem  earlier
                        if isGemFallsOut && checkEmptyElement(gameBoard [indexOfRow+numberOfStepsDown+1][indexOfColumn])
                        {
                            act.nextPosition.rowIndex = indexOfRow+numberOfStepsDown+1
                            gameBoard [indexOfRow+numberOfStepsDown+1][indexOfColumn] = elem
                            gameBoard [indexOfRow+numberOfStepsDown][indexOfColumn] = Element()
                        }
                        //if it was gem -- move it out from the board on last "row"
                        if elem as? GemElement != nil && act.nextPosition.rowIndex == numRows-1
                        {
                            act.nextPosition.rowIndex = numRows
                            gameBoard [indexOfRow+numberOfStepsDown][indexOfColumn] = Element()
                            gameBoard [indexOfRow][indexOfColumn] = Element()
                            //set flag to increment position for element
                            isGemFallsOut = true
                        }

                    }
                    else
                    {
                        act.nextPosition.rowIndex = numRows-1
                        gameBoard [numRows-1][indexOfColumn] = elem
                        gameBoard [indexOfRow][indexOfColumn] = Element()
                        //if we got rid of gem out earlier
                        if isGemFallsOut && checkEmptyElement(gameBoard [numRows-1][indexOfColumn])
                        {
                            act.nextPosition.rowIndex++
                            gameBoard [numRows-1][indexOfColumn] = elem
                            gameBoard [indexOfRow][indexOfColumn] = Element()
                        }
                        //if it was gem
                        if elem as? GemElement != nil
                        {
                            act.nextPosition.rowIndex = numRows
                            gameBoard [numRows-1][indexOfColumn] = Element()
                            gameBoard [indexOfRow][indexOfColumn] = Element()
                        }
                    }

                action.actionsArray.append(act)
                }
            }
            numberOfStepsDown = 0
        }
        action.sync = .Parallel
        return action
    }
    /**
     Create sequence of horizontal moves for elements
     
     @return ActionSequence of moving certain elements
     
     */
 func createHorizontalActions() ->ActionSequence
 {
    var numberOfStepsHorizontal: Int = 0
    let action = ActionSequence()
    
    for var indexOfRow = 0; indexOfRow < numRows; indexOfRow++
    {
        var rowWithGem = -1
        numberOfStepsHorizontal = 0
        //from left to right
        for var indexOfColumn = numCols/2-1; indexOfColumn >= 0; indexOfColumn--
        {
           
            if  (checkEmptyColumn(indexOfColumn))
            {
                numberOfStepsHorizontal++
            }
            else
                    if ( numberOfStepsHorizontal != 0)
                    {
                        print("count of empty: ", numberOfStepsHorizontal)
                        for var indexOfRowFromCurrent = indexOfRow; indexOfRowFromCurrent < numRows; indexOfRowFromCurrent++
                        {
                            let elem = gameBoard [indexOfRowFromCurrent][indexOfColumn]
                            if elem as? GemElement != nil
                            {
                                rowWithGem = indexOfRow
                            }
                            //if there is not empty element, not gem and we have position to move
                            if (checkNotEmptyElement(elem) && elem as? GemElement == nil &&
                                (indexOfRowFromCurrent != rowWithGem  || rowWithGem == -1)  &&
                                checkEmptyElement(gameBoard[indexOfRowFromCurrent][indexOfColumn+numberOfStepsHorizontal]))
                            {
                                let act = Action()
                                act.element = elem
                                act.type = .Move
                                act.nextPosition.rowIndex = indexOfRowFromCurrent
                        
                                if (indexOfColumn+numberOfStepsHorizontal<=numCols/2-1)
                                {
                                    act.nextPosition.colIndex = indexOfColumn+numberOfStepsHorizontal
                                    gameBoard [indexOfRowFromCurrent][indexOfColumn+numberOfStepsHorizontal] = elem
                                    gameBoard [indexOfRowFromCurrent][indexOfColumn] = Element()
                                }
                                else
                                {
                                    print("more than maximum");
                                    act.nextPosition.colIndex = numCols/2-1
                                    gameBoard [indexOfRowFromCurrent][numCols/2-1] = elem
                                    gameBoard [indexOfRowFromCurrent][indexOfColumn] = Element()
                                }
                                action.actionsArray.append(act)
                            }
                        }
  
                    }
            }
            numberOfStepsHorizontal = 0
    }
    //from right to left
    numberOfStepsHorizontal = 0
    for var indexOfRow = 0; indexOfRow < numRows; indexOfRow++
    {
        var rowWithGem = -1
        for var indexOfColumn = numCols/2; indexOfColumn < numCols; indexOfColumn++
        {
            if  (checkEmptyColumn(indexOfColumn))
            {
                numberOfStepsHorizontal++
            }
            else
                if ( numberOfStepsHorizontal != 0)
                {
                    for var indexOfRowFromCurrent = indexOfRow; indexOfRowFromCurrent < numRows; indexOfRowFromCurrent++
                    {
                        let elem = gameBoard [indexOfRowFromCurrent][indexOfColumn]
                        if elem as? GemElement != nil
                        {
                            rowWithGem = indexOfRowFromCurrent
                        }
                        if (checkNotEmptyElement(elem) && elem as? GemElement == nil &&
                            (indexOfRowFromCurrent != rowWithGem || rowWithGem == -1) &&
                            checkEmptyElement(gameBoard[indexOfRowFromCurrent][indexOfColumn-numberOfStepsHorizontal]))
                        {
                            let act = Action()
                            act.element = elem
                            act.type = .Move
                            act.nextPosition.rowIndex = indexOfRowFromCurrent
                            if (indexOfColumn-numberOfStepsHorizontal>=numCols/2)
                            {
                                act.nextPosition.colIndex = indexOfColumn-numberOfStepsHorizontal
                                gameBoard [indexOfRowFromCurrent][indexOfColumn-numberOfStepsHorizontal] = elem
                                gameBoard [indexOfRowFromCurrent][indexOfColumn] = Element()
                            }
                            else
                            {
                                print("more than maximum");
                                act.nextPosition.colIndex = numCols/2
                                gameBoard [indexOfRowFromCurrent][numCols/2] = elem
                                gameBoard [indexOfRowFromCurrent][indexOfColumn] = Element()
                            }
                            action.actionsArray.append(act)
                        }
               
                    }
                }

        }
        numberOfStepsHorizontal=0;
    }
    action.sync = .Parallel

    return action
    
 }
    /**
     Check element for non-emptyness
     
     @param element the element to check
     
     @return true or false state
     
     */
    func checkNotEmptyElement(element: Element) -> Bool
    {
        if ((element as? GemElement) != nil ||
            (element as? UnbreakableElement) != nil ||
            (element as? ChangeScissorsElement) != nil ||
            (element as? ChangeTypeElement) != nil ||
            (element as? ScissorsElement) != nil ||
            (element as? BombElement) != nil ||
            (element as? UsualElement) != nil)
        {
            return true
        }
        
        else
        {
            return false
        }
    }
    /**
     Check element for emptyness
     
     @param element the element to check
     
     @return true or false state
     
     */
    func checkEmptyElement(element: Element) -> Bool
    {
        if ((element as? GemElement) == nil &&
            (element as? UnbreakableElement) == nil &&
            (element as? ChangeScissorsElement) == nil &&
            (element as? ChangeTypeElement) == nil &&
            (element as? ScissorsElement) == nil &&
            (element as? BombElement) == nil &&
            (element as? UsualElement) == nil)
        {
            return true
        }
            
        else
        {
            return false
        }
    }
    /**
     Check if certain column is empty
     
     @param col the number of column to check
     
     @return true or false state
     
     */
    func checkEmptyColumn(indexOfColumn: Int) -> Bool
    {
        var countOfEmpty: Int = 0
        for var indexOfRow = 0; indexOfRow < numRows; indexOfRow++
        {
            if ((gameBoard [indexOfRow][indexOfColumn] as? GemElement) == nil &&
                (gameBoard [indexOfRow][indexOfColumn] as? UnbreakableElement) == nil &&
                (gameBoard [indexOfRow][indexOfColumn] as? ChangeScissorsElement) == nil &&
                (gameBoard [indexOfRow][indexOfColumn] as? ChangeTypeElement) == nil &&
                (gameBoard [indexOfRow][indexOfColumn] as? ScissorsElement) == nil &&
                (gameBoard [indexOfRow][indexOfColumn] as? BombElement) == nil &&
                (gameBoard [indexOfRow][indexOfColumn] as? UsualElement) == nil)
                {
                    countOfEmpty++
                }
        }
        
        if countOfEmpty == numRows
        {
            return true
        
        }
        else
        {
            return false
        
        }
    }
    /**
  Prints whole array in console
     
     */
    func printElem()
    {
        var str: String = String()
        for indexOfRow in 0...numRows-1
        {
            for indexOfColumn in 0...numCols-1
            {
                if let elem = gameBoard[indexOfRow][indexOfColumn] as? UsualElement
                {
                    str = str +  " " + String(elem.rowIndex) + "," + String(elem.colIndex) + " "
                }
                else
                if gameBoard[indexOfRow][indexOfColumn] as? GemElement != nil
                {
                    str = str + "  +  "
                }
                else
                if gameBoard[indexOfRow][indexOfColumn] as? BombElement != nil
                {
                    str = str + "  &  "
                }
                else
                if gameBoard[indexOfRow][indexOfColumn] as? ScissorsElement != nil
                {
                        str = str + "  X  "
                }
                else
                if gameBoard[indexOfRow][indexOfColumn] as? UnbreakableElement != nil
                {
                        str = str + "  #  "
                }
                else
                if  gameBoard[indexOfRow][indexOfColumn] as? ChangeTypeElement != nil
                {
                    str = str + "  ?  "
                }
                else
                if  gameBoard[indexOfRow][indexOfColumn] as? ChangeScissorsElement != nil
                {
                    str = str + "  @  "
                }
                else
                {
                    str = str + "  *  "
                }
                
            }
            str = str+"\n"
        }
        print(str)
    }
    /**
     Action of activation element that rotate scissors
     
     */
    func changeScissorsDirection()
    {
        for var indexOfRow = 0; indexOfRow < numRows; indexOfRow++
        {
            for var indexOfColumn = 0; indexOfColumn < numCols; indexOfColumn++
            {
                if let scissors = gameBoard[indexOfRow][indexOfColumn] as? ScissorsElement
                {
                    if scissors.direction == .right
                    {
                        scissors.direction = .down
                    }
                    else
                        if scissors.direction == .down
                        {
                            scissors.direction = .left
                        }
                        else
                            if scissors.direction == .left
                            {
                                scissors.direction = .up
                        
                            }
                            else
                                if scissors.direction == .up
                                {
                                    scissors.direction = .right
                        
                                }
                }
            }
        }
        
    }
    /**
     Action of activation element that changes all element types except itself and gems
     
     @param iPos row position of element in array
     
     @param jPos column position of element in array

     
     */
    func changeElementType(positionInRow: Int, positionInColumn: Int)
    {
        
        for var indexOfRow = 0; indexOfRow < numRows; indexOfRow++
        {
            for var indexOfColumn = 0; indexOfColumn < numCols; indexOfColumn++
            {
                if checkNotEmptyElement(gameBoard[indexOfRow][indexOfColumn]) && gameBoard[indexOfRow][indexOfColumn] as? GemElement == nil && gameBoard[indexOfRow][indexOfColumn] != gameBoard[positionInRow][positionInColumn]
                {
                    let str = generateRandomClass()
                    let newElement = Element.createWithType(str, size: gameBoard[indexOfRow][indexOfColumn].size)
                    newElement.colIndex = gameBoard[indexOfRow][indexOfColumn].colIndex
                    newElement.rowIndex = gameBoard[indexOfRow][indexOfColumn].rowIndex
                    newElement.position = gameBoard[indexOfRow][indexOfColumn].position
                    gameBoard[indexOfRow][indexOfColumn].parent?.addChild(newElement)
                    gameBoard[indexOfRow][indexOfColumn].removeFromParent()
                    gameBoard[indexOfRow][indexOfColumn] = newElement
                }
            }
        }
        
    }
    /**
     Create action sequence depending on selectedd element
     
     @param iPos row position of element in array
     
     @param jPos column position of element in array
     
     @return ActionSequence for certain elements that ready to be performed
     
     */
    func clickUsual(positionInRow: Int, positionInColumn:Int) -> ActionSequence
    {
        let sequence = ActionSequence()
        // if usual selected
       if (gameBoard[positionInRow][positionInColumn] as? UsualElement != nil)
       {
            checkElems(positionInRow, positionInColumn: positionInColumn)
       }
        //if bomb selected
        if (gameBoard[positionInRow][positionInColumn] as? BombElement != nil)
        {
            let bomb = bombActivate(positionInRow, positionInColumn: positionInColumn)
            if (bomb.sequenceArray.count != 0 || bomb.actionsArray.count != 0)
            {
                sequence.sequenceArray.append(bomb)
                print("Bomb Activate")
                printElem()
            }
        }
        // if scissors selected
        if (gameBoard[positionInRow][positionInColumn] as? ScissorsElement != nil)
        {
            let sciss = scissorsActivate(positionInRow, positionInColumn: positionInColumn)
            if (sciss.sequenceArray.count != 0 || sciss.actionsArray.count != 0)
            {
                sequence.sequenceArray.append(sciss)
                print("Scissors Activate ")
                printElem()
            }
        }
        //if scissors change selected
        if (gameBoard[positionInRow][positionInColumn] as? ChangeScissorsElement != nil)
        {
            changeScissorsDirection()
            let singleDeletion = ActionSequence()
            let action = Action()
            action.type = .Destroy
            action.element = gameBoard[positionInRow][positionInColumn]
            singleDeletion.actionsArray.append(action)
            sequence.sequenceArray.append(singleDeletion)
            gameBoard[positionInRow][positionInColumn]  = Element()
        }
        //if change type selected
        if (gameBoard[positionInRow][positionInColumn] as? ChangeTypeElement != nil)
        {
            changeElementType(positionInRow, positionInColumn:  positionInColumn)
            let singleDeletion = ActionSequence()
            let action = Action()
            action.type = .Destroy
            action.element = gameBoard[positionInRow][positionInColumn]
            singleDeletion.actionsArray.append(action)
            sequence.sequenceArray.append(singleDeletion)
            gameBoard[positionInRow][positionInColumn]  = Element()
        }
        // standart actions sequencse Down -- Horizontal -- Down
        let del = createDeleteActions()
        if (del.sequenceArray.count != 0 || del.actionsArray.count != 0)
        {
            sequence.sequenceArray.append(del)
            print("Deletion")
            printElem()
        }
        let down = createDownActions()
        if (down.sequenceArray.count != 0 || down.actionsArray.count != 0)
        {
            sequence.sequenceArray.append(down)
            print("Down")
            printElem()
        }
        let horizont = createHorizontalActions()
        if (horizont.sequenceArray.count != 0 || horizont.actionsArray.count != 0)
        {
            sequence.sequenceArray.append(horizont)
            print("Horizontal")
            printElem()
        }
        let down2 = createDownActions()
        if (down2.sequenceArray.count != 0 || down2.actionsArray.count != 0)
        {
            sequence.sequenceArray.append(down2)
            print("Down")
            printElem()
        }
        return sequence
    }
    /**
     Check state of gems
     
     @return true -- if there are no gems, false -- if there are at least one
     
     */
    func checkToWin()-> Bool
    {
        for var indexOfRow = 0; indexOfRow < numRows; indexOfRow++
        {
            for var indexOfColumn = 0; indexOfColumn < numCols; indexOfColumn++
            {
                if gameBoard[indexOfRow][indexOfColumn] as? GemElement != nil
                {
                    return false
                }
            }
        }
        return true
    }
    //to restart animations of change_type elements
    func restartTextureAnimation()
    {
        for var indexOfRow = 0; indexOfRow < numRows; indexOfRow++
        {
            for var indexOfColumn = 0; indexOfColumn < numCols; indexOfColumn++
            {
                if gameBoard[indexOfRow][indexOfColumn] as? ChangeTypeElement != nil
                {
                   gameBoard[indexOfRow][indexOfColumn].createTextureAnimationAndRun(AnimationAtlas.changeType.rawValue)
                }
                
            }
        }
        
    }
    func typeOfElement(element: Element) -> String
    {
        
        if element as? UsualElement != nil
        {
            return "UsualElement"
        }
        if element as? UnbreakableElement != nil
        {
            return "UnbreakableElement"
        }
        if element as? GemElement != nil
        {
            return "GemElement"
        }
        if element as? ChangeTypeElement != nil
        {
            return "ChangeTypeElement"
        }
        if element as? ChangeScissorsElement != nil
        {
            return "ChangeScissorsElement"
        }
        if element as? ScissorsElement != nil
        {
            return "ScissorsElement"
        }
        if element as? BombElement != nil
        {
            return "BombElement"
        }
        return ""
    }
    
    func resetArray()
    {
        gameBoard = Array<Array<Element>>()
        for _ in 0...numRows
        {
            gameBoard.append(Array(count:numCols, repeatedValue:Element()))
        }
        countOfTheSame = 0
        startingElement = nil
    }
    
    func existGemInColumn(column: Int) -> Bool
    {
        for var indexOfRow = 0; indexOfRow < numRows; indexOfRow++
        {
            if gameBoard[indexOfRow][column] as? GemElement != nil
            {
                return true
            }
       
        }
        return false
    }
}
