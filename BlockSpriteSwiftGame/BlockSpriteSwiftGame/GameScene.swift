//
//  GameScene.swift
//  BlockSpriteSwiftGame
//
//  Created by Admin on 1/20/16.
//  Copyright (c) 2016 Admin. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    var startPoint: CGPoint
    var elementSize: CGSize
    var rowCreationAction = SKAction()
    var isTouchesAllowed = true
    let timeToCreationNewRowInSeconds = 9.0
    var isOnPause = false
    var isLose = false
    var isWin = false
    var touchedUsualElement: UsualElement?
    // to resolve problem with row creation -- store last created row
    var delayedRowCreation: ActionSequence? = nil
    //
    func blurFilter () -> CIFilter
    {
        let filter = CIFilter(name: "CIBoxBlur")
        filter?.setDefaults()
        filter?.setValue(20, forKey: "inputRadius")
        return filter!
    }
    func addNewRow()
    {
//        if isTouchesAllowed
//        {
       // isTouchesAllowed = false
        let sprites = SpritesManager.sharedManager
        let  (sArray, aArray) = sprites.addRow(CGSize(width: elementSize.width, height: elementSize.height))
            if sArray.count != 0
            {
                for elem in sArray
                {
                elem.position = CGPoint(x: (CGFloat(elem.colIndex)*elem.size.width+startPoint.x), y: (CGFloat(elem.rowIndex)*elem.size.width+startPoint.y))
                self.addChild(elem)
                //print(elem)
                }
                
               if isTouchesAllowed
                {
                    isTouchesAllowed = false
                    delayedRowCreation = nil
                    let creation = self.actionForKey("rowCreation")
                    creation?.speed = 0
                    aArray.start({
                        self.isTouchesAllowed = true
                        creation?.speed = 1
                        print("row was created")
                    // self.checkWin()
                    })
                }
                else
                {
                    // action for delay
                   delayedRowCreation = aArray
                    let creation = self.actionForKey("rowCreation")
                    creation?.speed = 0
                    //
                }
                
            }
            else
            {
                print("You lose")
                ifLose()
            }
//        }
    }
    
    func startLevel()
    {
        let sprites = SpritesManager.sharedManager
        sprites.resetArray()
//        elementSize = CGSize(width: self.frame.width/CGFloat(sprites.numCols), height: self.frame.height/CGFloat(sprites.numCols))
//        startPoint = CGPoint(x: elementSize.width/2, y: self.frame.height-elementSize.height/2)
        
        self.removeAllActions()
        for node in self.children
        {
            if !(node.name == "Save" || node.name == "Load" || node.name == "Pause")
            {
                node.removeFromParent()
            }
        }
        //
        self.speed = 1
        self.paused = false
        delayedRowCreation = nil
        //
        var   (sArray, aArray) = sprites.addRow(CGSize(width: self.frame.width/8, height: self.frame.width/8), isGems: false)
        for elem in sArray
        {
            elem.position = CGPoint(x: (CGFloat(elem.colIndex)*elem.size.width+startPoint.x), y: (startPoint.y-CGFloat(elem.rowIndex)*elem.size.width))
            self.addChild(elem)
            //print(elem)
        }
        aArray.start({})
        //
        (sArray, aArray) = sprites.addRow(CGSize(width: self.frame.width/8, height: self.frame.width/8), isGems: false)
        for elem in sArray
        {
            elem.position = CGPoint(x: (CGFloat(elem.colIndex)*elem.size.width+startPoint.x), y: (startPoint.y-CGFloat(elem.rowIndex)*elem.size.width))
            self.addChild(elem)
            //print(elem)
        }
        aArray.start({})
        //
        (sArray, aArray) = sprites.addRow(CGSize(width: self.frame.width/8, height: self.frame.width/8), isGems: false)
        for elem in sArray
        {
            elem.position = CGPoint(x: (CGFloat(elem.colIndex)*elem.size.width+startPoint.x), y: (startPoint.y-CGFloat(elem.rowIndex)*elem.size.width))
            self.addChild(elem)
            //print(elem)
        }
        aArray.start({})
        //
        (sArray, aArray) = sprites.addRow(CGSize(width: self.frame.width/8, height: self.frame.width/8), isGems: true)
        for elem in sArray
        {
            elem.position = CGPoint(x: (CGFloat(elem.colIndex)*elem.size.width+startPoint.x), y: (startPoint.y-CGFloat(elem.rowIndex)*elem.size.width))
            self.addChild(elem)
            //print(elem)
        }
        aArray.start({self.isTouchesAllowed = true
        self.isOnPause = false
        self.isLose = false
        self.isWin = false})
        
        
        
        rowCreationAction = SKAction.repeatActionForever(SKAction.sequence([SKAction.waitForDuration(timeToCreationNewRowInSeconds), SKAction.runBlock({self.addNewRow()})]))
        //run action of creation row
         self.runAction(rowCreationAction, withKey: "rowCreation")
        

    }
    override func didMoveToView(view: SKView) {
    self.startLevel()
         createButtons()
        //to pause all animations at the scene
//        self.paused = true
//        self.speed = 0
        

        

//        let lightningNode = SKEffectNode()
//        let light = SKSpriteNode(imageNamed: "Spaceship")
//        light.blendMode = SKBlendMode.Add
//        lightningNode.addChild(light)
//        lightningNode.blendMode = SKBlendMode.Add
  //      lightningNode.filter = self.blurFilter()
       // myLabel.addChild(lightningNode)
        //
    //    self.addChild(myLabel)
  
     //   gsk = GamaManager.init()
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        let touch = touches.first!
        let touchLocation = touch.locationInNode(self)
        let touchedNode = self.nodeAtPoint(touchLocation)
        if  let elem = touchedNode as? UsualElement
        {
            if isTouchesAllowed
            {
                touchedUsualElement = elem
                touchedUsualElement?.onActivation()
            }
        }
        
        if touchedNode.name == "Save" && isTouchesAllowed
        {
             print("Save game")
             GameManager.sharedManager.saveLevel()
        }
        if touchedNode.name == "Win" || touchedNode.name == "Lose"
        {
            print("Restart game")
            // set score to 0
            GameManager.sharedManager.currentScore = 0
            //
       
            self.paused = false
            self.speed = 1
            touchedNode.runAction(SKAction.fadeOutWithDuration(0.5), completion: { self.startLevel()})
          
        }
        
        if touchedNode.name == "Load" && isTouchesAllowed
        {
            
             isTouchesAllowed = true
             print("Load game")
            if   GameManager.sharedManager.loadLevel(elementSize)
            {
                //delete all nodes
                for node in self.children
                {
                    if !(node.name == "Save" || node.name == "Load" || node.name == "Pause")
                    {
                        node.removeFromParent()
                    }
                }
                //replace elements
                for var i = 0; i < SpritesManager.sharedManager.numRows; i++
                {
                    for var j = 0; j < SpritesManager.sharedManager.numCols; j++
                    {
                        let element = SpritesManager.sharedManager.gameBoard[i][j]
                        if SpritesManager.sharedManager.checkNotEmptyElement(element)
                        {
                            element.position = CGPoint(
                                x: (CGFloat(element.colIndex)*element.size.width+startPoint.x),
                                y: (startPoint.y-CGFloat(element.rowIndex)*element.size.width))
                      
                            self.addChild(element)
                        }
                    }
                }
                //place missed gems
                for var j = 0; j < SpritesManager.sharedManager.numCols; j++
                {
                    if !SpritesManager.sharedManager.existGemInColumn(j)
                    {
                        let removedGem = Element.createWithType("GemElement", size: elementSize)
                        removedGem.colIndex = j
                        removedGem.rowIndex = SpritesManager.sharedManager.numRows
                        removedGem.position = CGPoint(
                            x: (CGFloat(removedGem.colIndex)*removedGem.size.width+startPoint.x),
                            y: (startPoint.y-CGFloat(removedGem.rowIndex)*removedGem.size.width))
                        self.addChild(removedGem)
                    }
                    
                }
            }
            
        }
        if touchedNode.name == "Pause" && !isWin && !isLose
        {
            print("Pause touched")
            if !isOnPause
            {
                
                self.paused = true
                self.speed = 0
                isOnPause = true
                isTouchesAllowed = false
            }
            else
            {
                self.paused = false
                self.speed = 1
                isOnPause = false
                isTouchesAllowed = true
            }
        }
        
    }
  
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        touchedUsualElement?.onActivation()
     
        let touch = touches.first!
        let touchLocation = touch.locationInNode(self)
        let touchedNode = self.nodeAtPoint(touchLocation)
        if let elem = touchedNode as? UsualElement
        {
            if isTouchesAllowed
            {
                
                let act =   SpritesManager.sharedManager.clickUsual(elem.rowIndex, positionInColumn: elem.colIndex)
            print(act.actionsArray.count )
                if act.actionsArray.count != 0 || act.sequenceArray.count != 0 // we need to check because there are must be deletable elements(not a single touch) to return game in normal state
                {
                    isTouchesAllowed = false
                    print("usual element touched")
               
                    let creation = self.actionForKey("rowCreation")
                    creation?.speed = 0 //pause action of creating row
                    act.start(({
                        if self.delayedRowCreation != nil
                        {
                            
                            self.delayedRowCreation?.start({
                                self.delayedRowCreation = nil
                                self.isTouchesAllowed = true
                                self.checkWin()
                                creation?.speed = 1 //start action of creating row again
                                print("completion 1")
                            })
                        }
                        else
                        {
                            self.isTouchesAllowed = true
                            self.checkWin()
                            creation?.speed = 1 //start action of creating row again
                            print("completion 2")
                        }
                    }))
                }
            }
     
        }
        if let elem = touchedNode as? BombElement
        {
            if isTouchesAllowed
            {
                isTouchesAllowed = false
                print("bomb element touched")
                    
                let act =   SpritesManager.sharedManager.clickUsual(elem.rowIndex, positionInColumn: elem.colIndex)
                let creation = self.actionForKey("rowCreation")
                creation?.speed = 0
                act.start(({
                        if self.delayedRowCreation != nil
                        {
                            self.delayedRowCreation?.start({
                                self.delayedRowCreation = nil
                                self.isTouchesAllowed = true
                                self.checkWin()
                                creation?.speed = 1
                            })
                        }
                        else
                        {
                            self.isTouchesAllowed = true
                            self.checkWin()
                            creation?.speed = 1
                        }
                }))

            }
            
        }
        
        if let elem = touchedNode as? ScissorsElement
        {
            if isTouchesAllowed
            {
                isTouchesAllowed = false
                print("scissors element touched")
                let act =   SpritesManager.sharedManager.clickUsual(elem.rowIndex, positionInColumn: elem.colIndex)
                let creation = self.actionForKey("rowCreation")
                creation?.speed = 0
                act.start(({
                    print("completion reached")
                    if self.delayedRowCreation != nil
                    {
                        self.delayedRowCreation?.start({
                            self.delayedRowCreation = nil
                            self.isTouchesAllowed = true
                            self.checkWin()
                            creation?.speed = 1
                        })
                    }
                    else
                    {
                        self.isTouchesAllowed = true
                        self.checkWin()
                        creation?.speed = 1
                    }
                }))
            }
            
        }
        if let elem = touchedNode as? ChangeScissorsElement
        {
            if isTouchesAllowed
            {
                isTouchesAllowed = false
                print("scissors change element touched")
                let act =   SpritesManager.sharedManager.clickUsual(elem.rowIndex, positionInColumn: elem.colIndex)
                let creation = self.actionForKey("rowCreation")
                creation?.speed = 0
                act.start(({
                    
                    if self.delayedRowCreation != nil
                    {
                        self.delayedRowCreation?.start({
                            self.delayedRowCreation = nil
                            self.isTouchesAllowed = true
                            self.checkWin()
                            creation?.speed = 1
                        })
                    }
                    else
                    {
                        self.isTouchesAllowed = true
                        self.checkWin()
                        creation?.speed = 1
                    }
                }))
            }
            
        }
        if let elem = touchedNode as? ChangeTypeElement
        {
            if isTouchesAllowed
            {
                isTouchesAllowed = false
                print("type change element touched")
                let act =   SpritesManager.sharedManager.clickUsual(elem.rowIndex, positionInColumn: elem.colIndex)
                let creation = self.actionForKey("rowCreation")
                creation?.speed = 0
                act.start(({
                    
                    if self.delayedRowCreation != nil
                    {
                        self.delayedRowCreation?.start({
                            self.delayedRowCreation = nil
                            self.isTouchesAllowed = true
                            self.checkWin()
                            creation?.speed = 1
                        })
                    }
                    else
                    {
                        self.isTouchesAllowed = true
                        self.checkWin()
                        creation?.speed = 1
                    }
                }))
            }
        }
       touchedUsualElement = nil
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
  
   override  init(size: CGSize) {
    let sprites = SpritesManager.sharedManager
    elementSize = CGSize(width: size.width/CGFloat(sprites.numCols), height: size.height/CGFloat(sprites.numCols))
    startPoint = CGPoint(x: elementSize.width/2, y: size.height)

    touchedUsualElement = nil
    super.init(size: size)
  


    }

   required init?(coder aDecoder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
   }
    func checkWin()
    {
       if SpritesManager.sharedManager.checkToWin()
       {
        print("Win!")
        self.paused = true
        self.speed = 0
        
        isTouchesAllowed = false
        let winLabel = SKLabelNode(fontNamed: "Chalkduster")
        winLabel.name = "Win"
        winLabel.text = "You win!"
        winLabel.fontSize = 45
        winLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        winLabel.zPosition = 20
      //  winLabel.frame.size = self.frame.si
        self.addChild(winLabel)
        isWin = true
        
        }
    }
    func ifLose()
    {
        
            print("Lose!")
            self.paused = true
            self.speed = 0
            self.removeActionForKey("rowCreation")
            isTouchesAllowed = false
            delayedRowCreation = nil
            let loseLabel = SKLabelNode(fontNamed: "Chalkduster")
            loseLabel.name = "Lose"
            loseLabel.text = "You lose. "+"\n"+" Try again!"
            loseLabel.fontSize = 20
            loseLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
            loseLabel.zPosition = 20
            loseLabel.color = UIColor.blueColor()
            loseLabel.colorBlendFactor = 0.5
            //  winLabel.frame.size = self.frame.si
            self.addChild(loseLabel)
            isLose = true
            
   
    }
    func createButtons()
    {
        let saveButton = SKLabelNode(fontNamed: "Chalkduster")
        saveButton.name = "Save"
        saveButton.text = "Save"
        saveButton.fontSize = 14
        saveButton.position = CGPointMake(CGRectGetMidX(self.frame), 20);
        
        self.addChild(saveButton)
        
        let loadButton = SKLabelNode(fontNamed: "Chalkduster")
        loadButton.name = "Load"
        loadButton.text = "Load"
        loadButton.fontSize = 14
        loadButton.position = CGPointMake(30, 20);
        self.addChild(loadButton)
        
        let pauseButton = SKLabelNode(fontNamed: "Chalkduster")
        pauseButton.name = "Pause"
        pauseButton.text = "Pause"
        pauseButton.fontSize = 14
        pauseButton.position = CGPointMake(CGRectGetMaxX(self.frame)-30, 20);
        self.addChild(pauseButton)
        // FirstTryButton prototype
        
//        let pause = FTButtonNode(normalTexture: SKTexture(imageNamed: "usual"), selectedTexture: SKTexture(imageNamed: "usual2"), disabledTexture: SKTexture(imageNamed: "usual"))
//        pause.position = CGPoint(x: 20, y: 500)
//        pause.textLabel.text = "None"
//        pause.setButtonAction(self, triggerEvent: .TouchUpInside, action: Selector("pauseTouch"))
//        self.addChild(pause)
    }
    func pauseTouch()
    {
        print("pause touched")
    }
    
}
